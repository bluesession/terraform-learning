provider "aws" {
  region     = "ap-south-1"
  access_key = "AKIATLO7CF76F3PN7OTF"
  secret_key = "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"

}
variable "subnet_cidr_block" {
  description = "subnet cidr block"
  default     = "10.0.10.0/24"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

variable "environment" {
  description = "deployment environment"
}

resource "aws_vpc" "devolpment-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name : var.environment
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.devolpment-vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = "ap-south-1a"
  tags = {
    Name : "subnet-1-dev"
  }

}

data "aws_vpc" "existing_vpc" {
  default = true

}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = data.aws_vpc.existing_vpc.id
  cidr_block        = "172.31.48.0/20"
  availability_zone = "ap-south-1a"
  tags = {
    Name : "subnet-2-default"
  }

}

output "dev-vpc-id" {
  value = aws_vpc.devolpment-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}
